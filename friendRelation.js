/**
 * Picswitch
 * friendRelation.js
 *
 * Author(s): Gibson Mulonga, Jiacong Xu
 * Created: Apr-18-2015
 *
 * Summary: This module handles all relationship processes, such as sending and
 * confirming friend requests, removing friend, and so on. It is up to the user
 * to call functions in this module; however, basic sanity checks are still in
 * place to prevent falsely adding a person.
 *
 * Note that this is only for friends already on Picswitch. For friends who
 * don't, send them a text message on user's own phone instead. Also, to find
 * out which numbers are associated with Picswitch accounts, call FRFindFriends.
 *
 * The add friend workflow should be:
 * 1. Send every number from user's contacts list to FRFindFriends.
 * 2. Save the returned dictionaries (phone numbers associated with accounts as
 *    keys and their user IDs as values). One dictionary is for contacts not yet
 *    added; one dictionary is for contacts who are already user's friends.
 * 3. Display table of contacts. If a contact has exactly one number associated
 *    with the returned dictionary from previous step, invite should be sent to
 *    that. Otherwise, let user choose which number for that contact to send
 *    request to. A corner case is probably a contact with multiple numbers,
 *    with one number friend with user, one that is not, and one not on
 *    picswitch at all. We can probably let user choose which number they want
 *    to invite (of course, the one already friends doesn't need to be shown),
 *    and handle the remaining two cases separately (text or cloud code).
 * 4. Call FRRequestSend with a list of user IDs to send the request to.
 *
 * In the add friend view, there should also be a list of sent friend requests.
 * This can be retrieved using FRSentRequests.
 *
 * Note: for the current implementation, sending multiple friend requests can be
 * done with a single call, while others (remove friend, handle request) should
 * be done with multiple calls (multiple requests from front end).
 */
  
  
/**
 * Loading modules
 */
push = require("cloud/pushNotification.js");
  
  
/**
 * Constants
 */
const FRRequestAction = {
  ACCEPT: 0,    // Request is accepted
  DECLINE: 1,   // Request is declined
  CANCEL: 2     // Sender cancelled request
};
  
  
/**
 * This function should be called when friend requests is sent by a user. We
 * require that sender is the maker of the request. In case the recipient has a
 * pending friend request with the user, they automatically become friends.
 * 
 * request.params.recipients (string[]): the recipient user IDs.
 * On Android use List<String>
 * This function returns a string telling how many friends requests were sent.
 */
Parse.Cloud.define("FRRequestSend", function(request, response) {
  // Checking whether there is an active user
  var sender = request.user;
    
  if (!sender) {
    response.error("Error: not signed in.");
  }
    
  // Call upon the power of the master key
  Parse.Cloud.useMasterKey();
    
  // Filter the list of recipient IDs. Specifically, the recipients must not be
  // already friends or self. We do two queries, first we find all recipients,
  // then we go through each of them to see if they have sent sender a request
  // already (friend them straight away in this case).
  var recipientIds = request.params.recipients;
  var query = new Parse.Query(Parse.User);
  query.containedIn("objectId", recipientIds); // fetches all recipients
  query.notEqualTo("friends", sender); // filters recipients who are friends
  query.notEqualTo("objectId", sender.id); // check on front end!!
   
  var onError = function(error) {
    response.error("Error: " + error.code + " " + error.message);
  };
    
  var onSuccess = function(users) {
    // Second query looks for pending friend requests
    var ownRequestsQuery = sender.relation("friendRequests").query();
     
    var onSuccess2 = function(ownRequests) {
      // Compare users and own requests and handle them accordingly.
      var friendsToBe = [], requestingFriends = [];
        
      for (var i = 0; i < users.length; i++) {
        user = users[i];
         
        // Whether we should friend those two straight away
        var shouldFriend = false;
          
        for (var i = 0; i < ownRequests.length; i++) {
          ownRequest = ownRequests[i];
          if (ownRequest.id == user.id) {
            shouldFriend = true;
            break;
          }
        }
          
        if (shouldFriend) {
          friendsToBe.push(user);
          var relation1 = user.relation("friends");
          var relation2 = sender.relation("friends");
          var relation3 = sender.relation("friendRequests");
          var relation4 = user.relation("sentRequests");
          relation1.add(sender);
          relation2.add(user);
          relation3.remove(user);
          relation4.remove(sender);
        } else {
          requestingFriends.push(user);
          var relation1 = user.relation("friendRequests");
          var relation2 = sender.relation("sentRequests");
          relation1.add(sender);
          relation2.add(user);
        }
          
        user.save();
      }
        
      sender.save();
        
      // Send push notification separately
      for (var i = 0; i < friendsToBe.length; i++) {
        user = friendsToBe[i];
        // Sending to both users!
        push.PNFriendRequestAccepted(sender, user);
        push.PNFriendRequestAccepted(user, sender);
      }
        
      if (requestingFriends.length > 0) {
        push.PNFriendRequestNew(sender, requestingFriends);
      }
        
      response.success("Sent " + users.length + " friend requests.");
    };
      
    ownRequestsQuery.find({success: onSuccess2, error: onError});
  };
    
  query.find({success: onSuccess, error: onError});
});
  

/**
 * This function handles friend requests via Facebook id. It first construct a
 * list of Parse user id from given Facebook id, then call the above function.
 * 
 * request.params.recipients (string[]): the recipient user Facebook IDs.
 * On Android use List<String>
 * This function returns a string telling how many friends requests were sent.
 */
Parse.Cloud.define("FRRequestSendFB", function(request, response) {
  // Checking whether there is an active user
  var sender = request.user;
    
  if (!sender) {
    response.error("Error: not signed in.");
  }
    
  // Call upon the power of the master key
  Parse.Cloud.useMasterKey();
    
  // Filter the list of recipient IDs. Specifically, the recipients must not be
  // already friends or self. We do two queries, first we find all recipients,
  // then we go through each of them to see if they have sent sender a request
  // already (friend them straight away in this case).
  var recipientIds = request.params.recipients;
  var query = new Parse.Query(Parse.User);
  query.containedIn("fbId", recipientIds); // fetches all recipients
  query.notEqualTo("friends", sender); // filters recipients who are friends
  query.notEqualTo("objectId", sender.id); // check on front end!!
   
  var onError = function(error) {
    response.error("Error: " + error.code + " " + error.message);
  };
    
  var onSuccess = function(users) {
    // Second query looks for pending friend requests
    var ownRequestsQuery = sender.relation("friendRequests").query();
     
    var onSuccess2 = function(ownRequests) {
      // Compare users and own requests and handle them accordingly.
      var friendsToBe = [], requestingFriends = [];
        
      for (var i = 0; i < users.length; i++) {
        user = users[i];
         
        // Whether we should friend those two straight away
        var shouldFriend = false;
          
        for (var i = 0; i < ownRequests.length; i++) {
          ownRequest = ownRequests[i];
          if (ownRequest.id == user.id) {
            shouldFriend = true;
            break;
          }
        }
          
        if (shouldFriend) {
          friendsToBe.push(user);
          var relation1 = user.relation("friends");
          var relation2 = sender.relation("friends");
          var relation3 = sender.relation("friendRequests");
          var relation4 = user.relation("sentRequests");
          relation1.add(sender);
          relation2.add(user);
          relation3.remove(user);
          relation4.remove(sender);
        } else {
          requestingFriends.push(user);
          var relation1 = user.relation("friendRequests");
          var relation2 = sender.relation("sentRequests");
          relation1.add(sender);
          relation2.add(user);
        }
          
        user.save();
      }
        
      sender.save();
        
      // Send push notification separately
      for (var i = 0; i < friendsToBe.length; i++) {
        user = friendsToBe[i];
        // Sending to both users!
        push.PNFriendRequestAccepted(sender, user);
        push.PNFriendRequestAccepted(user, sender);
      }
        
      if (requestingFriends.length > 0) {
        push.PNFriendRequestNew(sender, requestingFriends);
      }
        
      response.success("Sent " + users.length + " friend requests.");
    };
      
    ownRequestsQuery.find({success: onSuccess2, error: onError});
  };
    
  query.find({success: onSuccess, error: onError});
});
  
/**
 * This function should be called when we wish to act on a particular request.
 * A request is queried by the sender and recipient. One is determined by the
 * function caller, the other is passed in. For example, if caller is the friend
 * request sender, then pass in the recipient's ID as otherUserId. If an invalid
 * action is requested, nothing will be done.
 * 
 * request.params.otherUserId (string): the other user's ID.
 * request.params.action (FRRequestAction): the action that needs to be done.
 *
 * This function returns a string telling what action has been done.
 */
Parse.Cloud.define("FRRequestHandle", function(request, response) {
  // Checking whether there is an active user
  if (!request.user) {
    response.error("Error: not signed in.");
  }
  // alert("The action is: " + FRRequestAction.CANCEL);
  // Call upon the power of the master key
  Parse.Cloud.useMasterKey();
    
  // Fetch the other user
  var query = new Parse.Query(Parse.User);
  query.equalTo("objectId", request.params.otherUserId);
  // alert("The action is: " + FRRequestAction.CANCEL);
  var onSuccess = function(other) {
    // Determine whose relation we need to alter
    var sender, recipient;
     
    if (request.params.action == FRRequestAction.CANCEL) {
       
      // The friend request sender called us.
      sender = request.user;
      recipient = other;
    } else {
      sender = other;
      recipient = request.user;
    }
      
    // Alter relation based on passed in code.
    if (request.params.action == FRRequestAction.ACCEPT) {
      // Accept friend request
      var relation1 = recipient.relation("friends");
      var relation2 = sender.relation("friends");
      var relation3 = recipient.relation("friendRequests");
      var relation4 = sender.relation("sentRequests");
      relation1.add(sender);
      relation2.add(recipient);
      relation3.remove(sender);
      relation4.remove(recipient);
        
      recipient.save();
      sender.save();
        
      // The friend request recipient accepted sender's friend request.
      push.PNFriendRequestAccepted(recipient, sender);
        
      response.success("Friend request successfully accepted.");
    } else {
      // Remove friend request
      var relation1 = recipient.relation("friendRequests");
      var relation2 = sender.relation("sentRequests");
      relation1.remove(sender);
      relation2.remove(recipient);
        
      sender.save();
      recipient.save();
        
      response.success("Friend request successfully removed.");
    }
  };
    
  var onError = function(error) {
    response.error("Error: " + error.code + " " + error.message);
  };
    
  query.first({success:onSuccess, error:onError});
});
  
  
/**
 * This function should be called when user wants to remove a particular friend.
 * If they are not friends to begin with, nothing will be done.
 * 
 * request.params.otherUserId (string): the other user's ID.
 *
 * This function returns a string telling what action has been done.
 */
Parse.Cloud.define("FRRemoveFriend", function(request, response) {
  // Checking whether there is an active user
  var sender = request.user;
    
  if (!sender) {
    response.error("Error: not signed in.");
  }
    
  // Call upon the power of the master key
  Parse.Cloud.useMasterKey();
  // Grabbing other user
  var query = new Parse.Query(Parse.User);
  query.equalTo("objectId", request.params.otherUserId);
    
  var onSuccess = function(other) {
    var relation1 = sender.relation("friends");
    var relation2 = other.relation("friends");
    relation1.remove(other);
    relation2.remove(sender);
      
    sender.save();
    other.save();
        
    response.success("Friend successfully removed.");
  };
    
  var onError = function(error) {
    response.error("Error: " + error.code + " " + error.message);
  };
    
  query.first({success:onSuccess, error:onError});
});
  
  
/**
 * This function should be called when user wants to flag a user.
 * 
 * request.params.otherUserId (string): the other user's ID.
 *
 * This function returns a string telling what action has been done.
 */
Parse.Cloud.define("FRFlagFriend", function(request, response) {
  // Call upon the power of the master key
  Parse.Cloud.useMasterKey();
    
  // Grabbing other user and flag the user.
  var query =  new Parse.Query(Parse.User);
  query.equalTo("objectId", request.params.otherUserId);
    
  var onSuccess = function(other) {
    other.set("isFlagged", true);
      
    other.save();
      
    response.success("Friend successfully flagged.");
  };
    
  var onError = function(error) {
    response.error("Error: " + error.code + " " + error.message);
  };
    
  query.first({success:onSuccess, error:onError});
});
  
  
/**
 * This function should be called when parsing user's contacts. We check each
 * number to see if the associated user is on picswitch, and whether that user
 * is a friend of calling user.
 * 
 * request.params.numbers (string[]): a list of phone numbers, international but
 * without "+".
 * On Android use List<String>
 *
 * This function returns two dictionaries. Both have phone numbers as keys and
 * PFUser.objectId as value.
 * response.friends (object): users that are on picswitch and are friends.
 * response.others (object): users that are on picswitch but aren't friends yet.
 *
 * Use these two objects accordingly to layout UI for contacts.
 */
Parse.Cloud.define("FRCheckContacts", function(request, response) {
  // Checking whether there is an active user
  var sender = request.user;
    
  if (!sender) {
    response.error("Error: not signed in.");
  }
    
  // Grabbing all users with the given numbers
  var query = new Parse.Query(Parse.User);
  query.containedIn("phoneNumber", request.params.numbers);
  var counter=0;
  var onError = function(error) {
    response.error("Error: " + error.code + " " + error.message);
  };
    
  var onSuccess = function(users) {
    //alert("Retrieved these guys:" + users.length);
    // Grab all friends
    var friendsQuery = sender.relation("friends").query();
      
    var onSuccess2 = function(friends) {
      var friendIds= "";
        for(var i=0; i<friends.length; i++){
            friendIds +=friends[i].id;
        }
      var usersFriend = {}, usersOther = {};
        
      for (var i = 0; i < users.length; i++) {
        user = users[i];
         
        // Adding to appropriate array
         
        var phone = user.get("phoneNumber");
         
        if (friendIds.indexOf(user.id)!=-1) {
          counter +=1;
          usersFriend[phone] = user.id;
        } else {
          counter +=1;
          usersOther[phone] = user.id;
        }
       
      }
        
      response.success({friends:usersFriend, others:usersOther});
    };
     
    friendsQuery.find({success:onSuccess2, error:onError});
  };
    
  query.find({success:onSuccess, error:onError});
});


/**
 * This function should be called when parsing user's Facebook friends. We check
 * each Facebook id to see if the associated user is a friend of calling user.
 * 
 * request.params.facebookIds (string[]): a list of Facebook ids
 * On Android use List<String>
 *
 * This function returns an array containing Facebook ids that are friends with
 * user.
 * response.friends (object): users that are on picswitch and are friends.
 */
Parse.Cloud.define("FRCheckContactsFB", function(request, response) {
  // Checking whether there is an active user
  var sender = request.user;
    
  if (!sender) {
    response.error("Error: not signed in.");
  }
    
  // Grabbing all users with the given Facebook ids
  var query = sender.relation("friends").query();
  query.containedIn("fbId", request.params.facebookIds);
  
  var onError = function(error) {
    response.error("Error: " + error.code + " " + error.message);
  };
    
  var onSuccess = function(friends) {
    // Return a list of ids that are friends
    var usersFriend = [];
    
    for (var i = 0; i < friends.length; i++) {
      usersFriend.push(friends[i].fbId);
    }
    
    response.success({friends:usersFriend});
  };
    
  query.find({success:onSuccess, error:onError});
});

  
/**
 * This function should be called when displaying user's active friend requests.
 * This will be a simple query with no arguments.
 *
 * This function returns an array of user's active requests (user IDs).
 */
Parse.Cloud.define("FRSentRequests", function(request, response) {
  // Checking whether there is an active user
  var sender = request.user;
    
  if (!sender) {
    response.error("Error: not signed in.");
  }
    
  var query = sender.relation("sentRequests");
    
  var onSuccess = function(requests) {
    var uids = requests.map(function(usr) {return usr.id});
      
    response.success(uids);
  };
    
  var onError = function(error) {
    response.error("Error: " + error.code + " " + error.message);
  };
    
  query.find({success:onSuccess, error:onError});
});