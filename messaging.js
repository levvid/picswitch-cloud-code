/**
 * Picswitch
 * messaging.js
 *
 * Author(s): Jiacong Xu
 * Created: Apr-18-2015
 *
 * Summary: This module handles all messaging operations, including sending and
 * solving. It performs checks on data, senders and recipients, as well as
 * dispatching notifications accordingly.
 *
 * For the photo, we first convert it to a base 64 string, then send the string
 * over the network.
 */
 
/**
 * Loading modules
 */
push = require("cloud/pushNotification.js");
 
/**
 * This method posts a new message, as well as dispatching notifications.
 * 
 * request.params.recipientIds (string[]): the object ids for the recipients of
 * this message.
 * request.params.photo (string): photo data encoded in base 64 format.
 * request.params.caption1 (string): optional, the 1st caption for the puzzle.
 * request.params.caption2 (string): optional, the 2nd caption for the puzzle.
 * request.params.previewIndex (int): the index of the sliced image used for
 * preview. Index starts from top left and proceeds through each row.
 * request.params.puzzleSize (int): the size of the puzzle.
 * request.params.timeLimit (int): time limit for the puzzle.
 *
 * This returns whether the message has been sent successfully.
 */
Parse.Cloud.define("MSSend", function(request, response) {
  var sender = request.user;
   
  if (!sender) {
    response.error("Error: not signed in.");
  }
   
  // Constructing message object.
  var Messages = Parse.Object.extend("Messages");
  var puzzle = new Messages();
  var file = new Parse.File("image.jpg", {base64: request.params.photo});
  puzzle.set("photo", file);
  puzzle.set("firstCaption", request.params.caption1);
  puzzle.set("secondCaption", request.params.caption2);
  puzzle.set("previewIndex", request.params.previewIndex);
  puzzle.set("puzzleSize", request.params.puzzleSize);
  puzzle.set("timeLimit", request.params.timeLimit);
  puzzle.set("sender", sender);
   
  // Determine recipients
  var found = false;
  for (var i = 0; i < request.params.recipientIds.length; i++) {
    if (request.params.recipientIds[i] === sender.id) {
      found = true;
    }
  }
   
  var query = sender.relation("friends").query();
  query.containedIn("objectId", request.params.recipientIds);
   
  query.find({
    success: function(results) {
      // Add recipients
      var recipients = puzzle.relation("recipients");
      for (var i = 0; i < results.length; i++) { 
        recipients.add(results[i]);
      }
       
      if (found) {
        // Also add user
        recipients.add(sender);
        results.push(sender);
      }
       
      // Save puzzle
      puzzle.save(null, {
        success: function(puzzle) {
          // Send notification
          push.PNMessageNew(sender, results, request.params.caption1, puzzle.id);
           
          response.success("Puzzle posted successfully!");
        },
        error: function(gameScore, error) {
          // Execute any logic that should take place if the save fails.
          response.error('Failed to create puzzle! Error: ' + error.message);
        }
      });
    },
    error: function(error) {
      response.error("Error: " + error.code + " " + error.message);
    }
  });
});
 
 
/**
 * This method handles user responses to messages. It should be called when user
 * finishes a new puzzle from friend.
 * 
 * request.params.messageId (string): the object ids for the message
 * request.params.isSolved (bool): whether user solved the puzzle or skipped it
 * request.params.solveTime (float): time spent solving the puzzle
 * request.params.solveSteps (int): number of steps spent solving the puzzle.
 *
 * This returns whether the response is logged on the server.
 */
Parse.Cloud.define("MSRespond", function(request, response) {
  var sender = request.user;
   
  if (!sender) {
    response.error("Error: not signed in.");
  }
   
  // Make sure that user is a recipient of the message
  var Messages = Parse.Object.extend("Messages");
  var query = new Parse.Query(Messages);
  query.get(request.params.messageId, {
    success: function(message) {
      // Next we check whether user exists in the list
      message.relation("recipients").query.count({
        success: function(number) {
        // There are number instances of MyClass.
        },
 
        error: function(error) {
          response.error('User is not in the list of recipients!');
        }
      });
    },
   
    error: function(object, error) {
      // Couldn't find object. Return false
      response.error('No such message!');
    }
  });
   
  // Constructing messageResponse object.
  var MessageResponse = Parse.Object.extend("MessageResponse");
  var puzzleResponse = new MessageResponse();
  isSolved = request.params.isSolved;
  puzzleResponse.set("isSolved", isSolved);
   
  if (isSolved) {
     
  }
  puzzleResponse.set("photo", file);
  puzzle.set("firstCaption", request.params.caption1);
  puzzle.set("secondCaption", request.params.caption2);
  puzzle.set("previewIndex", request.params.previewIndex);
  puzzle.set("puzzleSize", request.params.puzzleSize);
  puzzle.set("timeLimit", request.params.timeLimit);
  puzzle.set("sender", sender);
   
  // Determine recipients
  var found = false;
  for (var i = 0; i < request.params.recipientIds.length; i++) {
    if (request.params.recipientIds[i] === sender.id) {
      found = true;
    }
  }
   
  var query = sender.relation("friends").query();
  query.containedIn("objectId", request.params.recipientIds);
   
  query.find({
    success: function(results) {
      // Add recipients
      var recipients = puzzle.relation("recipients");
      for (var i = 0; i < results.length; i++) { 
        recipients.add(results[i]);
      }
       
      if (found) {
        // Also add user
        recipients.add(sender);
      }
       
      // Save puzzle
      puzzle.save(null, {
        success: function(puzzle) {
          // Send notification
          push.PNMessageNew(sender, results, request.params.caption1);
           
          response.success("Puzzle posted successfully!");
        },
        error: function(gameScore, error) {
          // Execute any logic that should take place if the save fails.
          response.error('Failed to create puzzle! Error: ' + error.message);
        }
      });
    },
    error: function(error) {
      response.error("Error: " + error.code + " " + error.message);
    }
  });
});