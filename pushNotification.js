/**
 * Picswitch
 * pushNotification.js
 *
 * Author(s): Gibson Mulonga, Jiacong Xu
 * Created: Apr-18-2015
 *
 * Summary: This module handles all app push notifications through Parse's
 * push handler. At specific frontend events, such as new friend request, puzzle
 * from friend, and so on, this module should be notified in order to send out
 * correct notifications to affected users. A full list of events and their keys
 * is provided at the head of this module.
 *
 * Sending a notification should be as follows:
 * 1. Call the appropriate notification generating function with required
 *    arguments.
 * 2. Message is handled and sent!
 *
 * Notice that currently this module contains legacy code, and eventually all
 * functions should be called from within cloud code!
 */
 
/**
 * Constants
 */
const PNPushType = {
  MESSAGE_NEW: 0,
  FRIEND_REQUEST_NEW: 1,
  FRIEND_REQUEST_ACCEPTED: 2
};
 
 
/**
 * This function should be called when a new message is composed and sent.
 * 
 * sender (User): the puzzle sender.
 * recipients (User[]): the recipients of this push notification.
 * message (string): optional, the composed message.
 */
exports.PNMessageNew = function (sender, recipients, message, objectId) {
  // Formatting message
  var name = sender.get("firstName");
  var id = objectId;
  if (message.length == 0 || !message) {
    // Returning default message
    message = "New picswitch from " + name;
  } else if (message.length > 40) {
    // Truncate and add ellipses
    message = name + ' says: "' + message.substring(0, 28) + '..."';
  } else {
    // Display full message
    message = name + ' says: "' + message + '"';
  }
 
  //  if (message.length > 40) {
  // // Truncate and add a ...
  //   message = message.substring(0, 40) + "...\"";
  // }else if(message.length=="" || message==null || !message){
  //   message = "New PicSwitch";
  // }
  // else {
  //   message = message + "...\"";
  // }
 
  // Formatting push data
  var pushData = {
    alert: message, 
    type: PNPushType.MESSAGE_NEW,
    //title: name
    action: "com.picswitch.TabActivity",
    name: sender.get("firstName"),
    messageId: id
  };
   
   
   
   
  // Treating success and error
  var onSuccess = function () {
    // Do nothing
  };
   
  var onError = function () {
    console.log("Push failed. Message: " + message);
  };
   
  // Finding devices
  var query = new Parse.Query(Parse.Installation);
  var usernames = recipients.map(function(usr) {return usr.getUsername()});
  
  query.containedIn("username", usernames);
   
  // Sending push notification
  Parse.Push.send({where: query, data: pushData}).then(onSuccess, onError);
}
 
 
/**
 * This function should be called when a batch of friend requests is sent.
 * 
 * sender (User): the friend request sender.
 * recipients (User[]): the recipients of this push notification.
 */
exports.PNFriendRequestNew = function (sender, recipients) {
  // Formatting message
  var message = sender.get("firstName") + " sent you a friend request!";
   
  // Formatting push data
  var pushData = {
    alert: message, 
    type: PNPushType.FRIEND_REQUEST_NEW,
    action: "com.picswitch.TabActivity",
    name: sender.get("firstName")
  };
   
  // Treating success and error
  var onSuccess = function () {
    // Do nothing
  };
   
  var onError = function () {
    console.log("Push failed. Message: " + message);
  };
   
  // Finding devices
  var query = new Parse.Query(Parse.Installation);
  var usernames = recipients.map(function(usr) {return usr.get("username")});
  query.containedIn("username", usernames);
   
  // Sending push notification
  Parse.Push.send({where: query, data: pushData}).then(onSuccess, onError);
}
 
 
/**
 * This function should be called when a friend request is accepted.
 * 
 * sender (User): the person who accepted the request.
 * recipient (User): the recipient of this push notification.
 */
exports.PNFriendRequestAccepted = function (sender, recipient) {
  // Formatting message
  var message = sender.get("firstName") + " has accepted your friend request!";
   
  // Formatting push data
  var pushData = {alert: message, type: PNPushType.FRIEND_REQUEST_ACCEPTED};
  // var pushData = {
  //   alert: message, 
  //   type: PNPushType.MESSAGE_NEW,
  //   title: senderUsername
  // };
  // Treating success and error
  var onSuccess = function () {
    // Do nothing
  };
   
  var onError = function () {
    console.log("Push failed. Message: " + message);
  };
   
  // Finding devices
  var query = new Parse.Query(Parse.Installation);
  query.equalTo("username", recipient.getUsername());
   
  // Sending push notification
  Parse.Push.send({where: query, data: pushData}).then(onSuccess, onError);
}
 
 
/**
 * Legacy code
 */
 
/**Parse Cloud function for sending push notifications to users
*
*recipients: list of recipients
*message: the first caption*/
Parse.Cloud.define("sendPushToUser", function(request, response) {
    if (!request.user) {
        response.error("Must be signed in to call this Cloud Function sendPushToUser.");
        return;
    }
 
  var sender = request.user;
  senderUsername = sender.get("firstName");
  var recipients = request.params.recipients;
  var message = request.params.message;
  
  // Validate that the sender is allowed to send to the recipient.
  // For example each user has an array of objectIds of friends
  
  // Validate the message text.
  // For example make sure it is under 140 characters
  if (message.length > 40) {
  // Truncate and add a ...
    message = message.substring(0, 40) + "...\"";
  }else if(message.length==""){
    message = "New PicSwitch";
    }
    else if(message==null){
        message = "New PicSwitch";
    }
    else {
        message = message + "...\"";
    }
     
   
  // Send the push.
  // Find devices associated with the recipient user
  // var recipientUser = new Parse.User();
  // recipientUser.username = recipientUsername;
  // var pushQuery = new Parse.Query(Parse.Installation);
  // pushQuery.equalTo("username", recipientUsername);
   
  // // Send the push notification to results of the query
  // Parse.Push.send({
  //   where: pushQuery,
  //   data: {
  //     alert: message
  //   }
  // }).then(function() {
  //     response.success("Push was sent successfully. " + message)
  // }, function(error) {
  //     response.error("Push failed to send with error: " + error.message);
  // });
 
 
// Formatting push data
  var pushData = {
    alert: message, 
    type: PNPushType.MESSAGE_NEW,
    title: senderUsername
  };
   
  // Treating success and error
  var onSuccess = function () {
    // Do nothing
  };
   
  var onError = function () {
    console.log("Push failed. Message: " + message);
  };
   
  // Finding devices
  var query = new Parse.Query(Parse.Installation);
  query.containedIn("username", recipients);
   
  // Sending push notification
  Parse.Push.send({where: query, data: pushData}).then(onSuccess, onError);
 
});