// Capture author info & user status
/*additional puzzles*/
Map<String, String> statistics = new HashMap<String, String>();
statistics.put("Puzzle_Type", "Additional");
statistics.put("Category", "category name");
//for the time spent, these are the categories:
statistics.put("Time_Spent", "0 - 10 seconds"); 
statistics.put("Time_Spent", "11 - 20 seconds"); 
statistics.put("Time_Spent", "21 - 30 seconds"); 
statistics.put("Time_Spent", "31 - 40 seconds"); 
statistics.put("Time_Spent", ">40 seconds"); 
 
 //time of day that the puzzle was done
statistics.put("Time_Of_Day", "Morning");  //5 am - 11 am
statistics.put("Time_Of_Day", "Afternoon");  //11 am - 4 pm
statistics.put("Time_Of_Day", "Evening");  //4 pm - 11 pm
statistics.put("Time_Of_Day", "Late Hours");  //11 pm - 5 am
//Log the timed event when the user starts reading the article
//setting the third param to true creates a timed event
FlurryAgent.logEvent("Additional_Puzzles_Solved", statistics, true);


/*additional puzzles skips*/
Map<String, String> statistics = new HashMap<String, String>();
statistics.put("Puzzle_Type", "Additional");
statistics.put("Category", "category name");
 
 //time of day that the puzzle was done
statistics.put("Time_Of_Day", "Morning");  //5 am - 11 am
statistics.put("Time_Of_Day", "Afternoon");  //11 am - 4 pm
statistics.put("Time_Of_Day", "Evening");  //4 pm - 11 pm
statistics.put("Time_Of_Day", "Late Hours");  //11 pm - 5 am
//Log the timed event when the user starts reading the article
//setting the third param to true creates a timed event
FlurryAgent.logEvent("Additional_Puzzles_Skipped", statistics, true);




/*Featured puzzles*/
Map<String, String> statistics = new HashMap<String, String>();
statistics.put("Puzzle_Type", "Featured");
statistics.put("Company_Name", "name");
//for the time spent, these are the categories:
statistics.put("Time_Spent", "0 - 10 seconds"); 
statistics.put("Time_Spent", "11 - 20 seconds"); 
statistics.put("Time_Spent", "21 - 30 seconds"); 
statistics.put("Time_Spent", "31 - 40 seconds"); 
statistics.put("Time_Spent", ">40 seconds"); 

//time of day that the puzzle was done
statistics.put("Time_Of_Day", "Morning");  //5 am - 11 am
statistics.put("Time_Of_Day", "Afternoon");  //11 am - 4 pm
statistics.put("Time_Of_Day", "Evening");  //4 pm - 11 pm
statistics.put("Time_Of_Day", "Late Hours");  //11 pm - 5 am
 
//Log the timed event when the user starts reading the article
//setting the third param to true creates a timed event
FlurryAgent.logEvent("Featured_Puzzles", statistics, true);






/*New puzzles solved*/
Map<String, String> statistics = new HashMap<String, String>();
statistics.put("Puzzle_Type", "New");
//for the time spent, these are the categories:
statistics.put("Time_Spent", "0 - 10 seconds"); 
statistics.put("Time_Spent", "11 - 20 seconds"); 
statistics.put("Time_Spent", "21 - 30 seconds"); 
statistics.put("Time_Spent", "31 - 40 seconds"); 
statistics.put("Time_Spent", ">40 seconds"); 
 

 //Replies
statistics.put("Replies", "Replied");
statistics.put("Replies", "Not Replied");

 //time of day that the puzzle was done
statistics.put("Time_Of_Day", "Morning");  //5 am - 11 am
statistics.put("Time_Of_Day", "Afternoon");  //11 am - 4 pm
statistics.put("Time_Of_Day", "Evening");  //4 pm - 11 pm
statistics.put("Time_Of_Day", "Late Hours");  //11 pm - 5 am

//Log the timed event when the user starts reading the article
//setting the third param to true creates a timed event
FlurryAgent.logEvent("New_Puzzles_Solved", statistics, true);


/*New puzzles sent*/
Map<String, String> statistics = new HashMap<String, String>();
statistics.put("Puzzle_Type", "New");
//for the time spent, these are the categories:
statistics.put("Time_Limit_Selected", "Easy"); 
statistics.put("Time_Limit_Selected", "Normal"); 
statistics.put("Time_Limit_Selected", "Hard"); 
statistics.put("Time_Limit_Selected", "Very Hard"); 
statistics.put("Time_Limit_Selected", "No Limit"); 
 

 //Replies
statistics.put("Replies", "Replied");
statistics.put("Replies", "Not Replied");

//Captions
statistics.put("Captions", "No First Caption"); //second caption present
statistics.put("Captions", "No Second Caption"); //first caption present
statistics.put("Captions", "Both Captions Present");


statistics.put("Image Source", "Gallery");
statistics.put("Image Source", "Camera");

 //time of day that the puzzle was done
statistics.put("Time_Of_Day", "Morning");  //5 am - 11 am
statistics.put("Time_Of_Day", "Afternoon");  //11 am - 4 pm
statistics.put("Time_Of_Day", "Evening");  //4 pm - 11 pm
statistics.put("Time_Of_Day", "Late Hours");  //11 pm - 5 am

//Log the timed event when the user starts reading the article
//setting the third param to true creates a timed event
FlurryAgent.logEvent("New_Puzzles_Sent", statistics, true);



