/***********************************************************************************************
*  PICSWITCH TECHNOLOGIES CONFIDENTIAL
*  ___________________________________
* 
*  PICSWITCH TECHNOLOGIES INC. 
*  Copyright (c) 2014, 2015 All Right Reserved.
* 
*
* Program           : Featured Statistics
*
* Author            : Gibson Levvid Mulonga
*
* Date created      : 07072015
*
* Purpose           : Handle statistics for featured puzzles
*
************************************************************************************************/
  
 
 
/**Function to add the user to the featured puzzles table users list 
  
/**Function to add the user as a recipient to our tutorial messageId
*Parameters: 
*puzzleId = the object Id of the featured puzzle;
*time = time taken;
*steps = number of steps taken**/ 
Parse.Cloud.define("featuredStatistics", function(request, status) {
     if (!request.user) {
        response.error("Must be signed in to call Cloud function Featured Statistics.");
        return;
    }
Parse.Cloud.useMasterKey();
 
var newUser = new Parse.User;
newUser.id = request.user.id;
var puzzleId = request.params.puzzleId;
var time = request.params.time;
var steps = request.params.steps;
      
var FeaturedStatistics = Parse.Object.extend("FeaturedStatistics");
var statsEntry = new FeaturedStatistics();
var puzzleFeatured = Parse.Object.extend("FeaturedPuzzles");
var puzzle = new puzzleFeatured();
puzzle.id = puzzleId;
newUser.addUnique("featuredSolved", puzzle.id);
statsEntry.set("user", newUser);
statsEntry.set("puzzle", puzzle);
statsEntry.set("time", time);
statsEntry.set("steps", steps);
// saving a record to the database
statsEntry.save();
newUser.save();
var stats = Parse.Object.extend("Statistics");
var query = new Parse.Query(stats);
query.equalTo("userPointer", newUser);
query.find({
  success: function(user) {
    // Successfully retrieved the user.
    if(user.length>0){
         
        var numberOfFeatured = user[0].get("numberOfFeatured");
 
        if(numberOfFeatured==null){
            numberOfFeatured = 0;
        }
        var points = user[0].get("points");
        if(points==null){
            points = 0;
        }
        user[0].set("points", points+5);
 
        var averageSteps = user[0].get("featuredAverageSteps");
        if(averageSteps==null){
            averageSteps = 0;
        }
        user[0].set("featuredAverageSteps", (averageSteps*numberOfFeatured+steps)/(numberOfFeatured+1));
 
        var averageTime = user[0].get("featuredAverageTime");
        if(averageTime==null){
            averageTime = 0;
        }
        user[0].set("featuredAverageTime", (averageTime*numberOfFeatured+time)/(numberOfFeatured+1));
        user[0].increment("numberOfFeatured");
 
        /**Save the three objects created
        *Array used to make sure all three are saved
        *Update to this module will include promises*/
        var objectToBeSavedArray = [];
        objectToBeSavedArray.push(statsEntry);
        objectToBeSavedArray.push(newUSer);
        objectToBeSavedArray.push(user[0]);
        Parse.Object.saveAll(objectToBeSavedArray, {
            success: function(objs) {
                status.success("Completed featured stats for user " + request.user.id);
            },
            error: function(error) { 
                // an error occurred...
            }
        });
         
    }else{
        var newUSer = Parse.Object.extend("Statistics");
        var user = new newUSer();
        user.set("points", 5);
        user.set("numberOfFeatured", 1);
        user.set("featuredAverageTime", time);
        user.set("featuredAverageSteps", steps);
        user.set("userPointer", request.user);
        user.save();
        status.success("Completed featured stats for user " + request.user.id);
    }
     
     
  },
  error: function(error) {
    alert("Error: " + error.code + " " + error.message);
  }
 
});
 
 
 
 
 
 
 
 
 
 
});
 
 
 
 
 
// Parse.Cloud.define("featuredStatistics", function(request, response) {
//  //make sure the user is logged in
//     if (!request.user) {
//         response.error("Must be signed in to call Cloud function Featured Statistics.");
//         return;
//     }
//     Parse.Cloud.useMasterKey();
//     //Puzzle Category points
//     var puzzleId = request.params.puzzleId;
//  var time = request.params.time;
//  var steps = request.params.steps;
      
//     //setting up the query
//     var featuredStatistics = Parse.Object.extend("FeaturedStatistics");
//  var userPuzzleEntry = new featuredStatistics();
//  userPuzzleEntry.set("user", request.user);
//  userPuzzleEntry.set("puzzle", puzzleId);
//  userPuzzleEntry.set("time", time);
//  userPuzzleEntry.set("steps", steps);
//     userPuzzleEntry.save(null,{
//  success:function(userPuzzleEntry) { 
//         updateFeaturedArray(request.user.id, puzzleId, response);
//         updateStatsTable(request.user.id, response);
//         response.success(userPuzzleEntry);
//  },
//  error:function(error) {
//     response.error(error);
//  }
//  });
 
 
    
// }); 
 
 
 
function updateFeaturedArray(userID, puzzleId, response) {
  var query = new Parse.Query(Parse.User);
    query.equalTo("objectId",  userID);
    query.first({
    success: function(user) {
        user.add("featuredSolved", puzzleId);
        // Save the user.
        user.save(null, {
        success: function(message) {
            // The user was saved successfully.
            response.success("Successfully updated user " + request.user.id);
        },
        error: function(message, error) {
            // The save failed.
            // error is a Parse.Error with an error code and description.
            response.error("Could not save changes to user " + request.user.id);
        }
      });
       
    },
    error: function(error) {
        //couldn't find the user so created new entry in Statistics
        response.error("Could not find user with user " + request.user.id);
    }
  });
}
 
 
function updateStatsTable(userID, response){
    //update stats table
    var stats = Parse.Object.extend("Statistics");
    var userStats = new Parse.Query(stats); //stats query
    userStats.equalTo("userPointer",  userID);
    userStats.find({
    success: function(user) {
        if(user.length>0){
            user[0].increment("numberOfFeatured");
            var points = user[0].get("points");
            user[0].set("points", points+5);
        }else{
            var newUser = Parse.Object.extend("Statistics");
            var user = new newUser();
            user.set("points", 5);
            user.set("numberOfFeatured", 1);
            user.save(null,{
              success: function(object) { 
                response.success(object.id);  // or just object, to send the whole object back.
                 
              },
              error: function(error) { 
                response.error(error);
              }
            });
 
        }
         
         
         
 
        // Save the user.
        user.save(null, {
        success: function(message) {
            // The user was saved successfully.
            response.success("Successfully updated user " + request.user.id);
        },
        error: function(message, error) {
            // The save failed.
            // error is a Parse.Error with an error code and description.
            response.error("Could not save changes to user " + request.user.id);
        }
      });
       
    },
    error: function(error) {
        //couldn't find the user so created new entry in Statistics
        response.error("Could not find user with user " + request.user.id);
    }
  });
}