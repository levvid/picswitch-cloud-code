
/**
*Cloud fucntion for sign up
*Returns true if the signup is successful
*Parameters:
*	: username - the user's username
*	: password - user's password
*	: email - user's email which is also the username
*	: age - user's age
*	: phonenumber - user's phonenumber
*	: firstname - user's first name
*	: lastname - user's last name
*	: gender - user's sex
*
*Returns: true if sign up successful*/

Parse.Cloud.define("signup", function(request, response) {
  	var user = new Parse.User();
  	var email = "" + request.params.email + "";
    email = email.replace(/\s+/g, ''); 
  	alert(email);
	user.set("username", email);
	user.set("password", request.params.password);
	user.set("email", email);
	user.set("age", request.params.age);
	user.set("phoneNumber", request.params.phonenumber);
	user.set("firstName", request.params.firstname);
	user.set("lastName", request.params.lastname);
	user.set("gender", request.params.gender);

	user.signUp(null, {
  	success: function(user) {
    	response.success(true);
  	},
  	error: function(user, error) {
  		response.error(error);
    	alert("Error: " + error.code + " " + error.message);
  	}
	});

});




Parse.Cloud.define("signupNew", function(request, response) {
    var user = new Parse.User();
    var email = "" + request.params.email + "";
    email = email.replace(/\s+/g, ''); 
    alert(email);
  user.set("username", email);
  user.set("password", request.params.password);
  user.set("email", email);
  user.set("age", request.params.age);
  user.set("firstName", request.params.firstname);
  user.set("lastName", request.params.lastname);
  user.set("gender", request.params.gender);
  user.set("picswitchUsername", request.params.picswitchUsername);
  user.set("installationId", request.params.installationId);
  user.signUp(null, {
    success: function(user) {
      response.success(true);
    },
    error: function(user, error) {
      response.error(error);
      alert("Error: " + error.code + " " + error.message);
    }
  });

});


/*Cloud function for logging in
*
*
*Parameters:
*	: username - the user's username
*	: password - user's password
*
*
*
*Returns true is the login is successful*/


Parse.Cloud.define("login", function(request, response) {
  	Parse.User.logIn(request.params.username, request.params.password, {
  	success: function(user) {
    	response.success(true);
  	},
  	error: function(user, error) {
    	response.error(error);
  	}
	});

});