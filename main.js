/***********************************************************************************************
*  PICSWITCH TECHNOLOGIES CONFIDENTIAL
*  ___________________________________
* 
*  PICSWITCH TECHNOLOGIES INC. 
*  Copyright (c) 2014, 2015 All Right Reserved.
* 
*
* Program           : PicSwitch Cloud Functions
*
* Author            : Gibson Levvid Mulonga
*
* Date created      : 20140915
*
* Purpose           : Handle push notifications
*                   : Handle PicSwitch Statistics for user puzzles
*                   : Make useful data available in a timely manner for advertisers
*                   : Track the preferences of our user base 
*                   : Handle featured puzzles statistics 
*
* 
*
* Revision History  :
*
* Version       Date(DDMMYYYY format)       Author                  Revision  
* v73           17032015                    Gibson Levvid Mulonga   Adding very hard(<10s) category to existing easy, medium and hard
* v81           21032015                    Gibson Levvid Mulonga   Switching to pointers to user class instead of usernames
* v82           21032015                    Gibson Levvid Mulonga   Commenting out the alert after stats query to make Logs easier on the eye
* v109          16042015                    Gibson Levvid Mulonga   Adding stats jobs and functions (some in new file)
* v110          16042015                    Gibson Levvid Mulonga   Adding friend confirmation
************************************************************************************************/
  
/**
 * Loading modules
 */
require("cloud/phoneVerification.js");
require("cloud/featuredRanking.js");
require("cloud/pushNotification.js");
require("cloud/messaging.js");
require("cloud/friendRelation.js");
require("cloud/tester.js");
require("cloud/featuredStatistics.js");
require("cloud/messageCleanUp.js");
require("cloud/userAccounts.js");
 
  
/**cloud function for handling statistics for new puzzles
*The following function updates the statistics table
*Parameters with names: puzzleDifficulty    - Either noLimit, easy, normal, hard or veryHard
*                       switches            - number of switches
*                       time                - time taken
*                       userPointer         - user object ID
***************************************************************************************************
*/
 
 
 
/**Cloud function to handle additional puzzles statistics
*Paramaters: puzzleDifficulty - one of easy, normal, hard, veryHard or noLimit or additional**
*            switches - number of switches
*            time - time taken
*/
Parse.Cloud.define("statistics", function(request, response) {
    //make sure the user is logged in
    if (!request.user) {
        response.error("Must be signed in to call Cloud function statistics.");
        return;
    }
    Parse.Cloud.useMasterKey();
    //Puzzle Category points
    var additionalPoints = 1;
    var noLimitPoints = 1;
    var easyPoints = 2;
    var normalPoints = 3;
    var hardPoints = 4;
    var veryHardPoints = 5;
 
      
      
    //Statistics variables from user
    var puzzleDifficulty = request.params.puzzleDifficulty;
    var switches = request.params.switches;
    var time = request.params.time;
    var userPointer = request.user;
      
    //setting up the query
    var statistics = Parse.Object.extend("Statistics");
    var query = new Parse.Query(statistics);
    //var puzzleCategory = request.params.category;
    var targetUser= new Parse.User();
    targetUser = userPointer;
    query.include("userPointer");
    query.equalTo("userPointer",  userPointer);
    query.find({
    success: function(userStats) {
        // Successfully retrieved the user.
        // Modify the parameters as necessary.
        // You can use request.params to pass specific
        // keys and values you might want to change about
        // this user.
          
        //statistics variables from statistics table
       // alert("Retrieved:" + userStats.id);
       var objectToBeSavedArray = [];
        if(userStats.length>0) {
            var newUserPointer = new Parse.User;
            newUserPointer.id = request.user.id;
            newUserPointer.set("stats", userStats[0].get("objectId"));
            objectToBeSavedArray.push(newUserPointer);
             
 
            var easy = userStats[0].get("easy"); //the user's current points
            if(easy==null){
                easy=0;
            }
            var noLimit= userStats[0].get("noLimit"); //the number of puzzles that the user has solved
            if(noLimit==null){
                noLimit=0;
            }
            var normal= userStats[0].get("normal"); //the number of puzzles that the user has solved
            if(normal == null){
                normal=0;
            }
            var hard= userStats[0].get("hard"); //the number of puzzles that the user has solved
            if(hard == null){
                hard=0;
            }
            var veryHard= userStats[0].get("veryHard"); //the number of puzzles that the user has solved
            if(veryHard == null){
                veryHard=0;
            }
            var points = userStats[0].get("points");
            if(points == null){
                points = 0;
            }
            var numberOfNew = userStats[0].get("numberOfNew");
            if(numberOfNew == null){
                numberOfNew = 0;
            }
 
              
            var numberOfAdditional = userStats[0].get("numberOfAdditional");
            if(numberOfAdditional == null){
                numberOfAdditional = 0;
            }
             
 
            //averages
            var averageSteps = userStats[0].get("averageSteps");
            if(averageSteps == null){
                averageSteps = 0;
            }
            averageSteps = ((averageSteps*numberOfAdditional) + switches)/(numberOfAdditional + 1);
            if(averageSteps == null || averageSteps<1){//
                averageSteps = 0;
            }
            var averageTime = userStats[0].get("averageTime");
            if(averageTime == null){
                averageTime = 0;
            }
            averageTime = ((averageTime*numberOfAdditional) + switches)/(numberOfAdditional + 1);
            if(averageTime == null || averageTime<1){
                averageTime = 0;
            }
 
 
            if(puzzleDifficulty=="easy"){
                userStats[0].increment("easy");
                userStats[0].set("points", points + easyPoints);
                userStats[0].set("numberOfNew", numberOfNew + 1);
            // Save the user.
                // userStats[0].save();
                // response.success("Successfully updated stats for user " + request.user.id);
            }
              
            else if(puzzleDifficulty=="hard"){
                userStats[0].increment("hard");
                userStats[0].set("points", points + hardPoints);
                userStats[0].set("numberOfNew", numberOfNew + 1);
            // Save the user.
                // userStats[0].save();
                // response.success("Successfully updated stats for user " + request.user.id);
            }
            else if(puzzleDifficulty == "noLimit"){
                userStats[0].increment("noLimit");
                userStats[0].set("points", points + noLimitPoints);
                userStats[0].set("numberOfNew", numberOfNew + 1);
            // Save the user.
                // userStats[0].save();
                // response.success("Successfully updated stats for user " + request.user.id);
            }
             else if(puzzleDifficulty == "normal"){
                userStats[0].increment("normal");
                userStats[0].set("points", points + normalPoints);
                userStats[0].set("numberOfNew", numberOfNew + 1);
            // Save the user.
                // userStats[0].save();
                // response.success("Successfully updated stats for user " + request.user.id);
            }
             
            else if(puzzleDifficulty=="veryHard"){
                userStats[0].increment("veryHard");
                userStats[0].set("points", points + veryHardPoints);
                userStats[0].set("numberOfNew", numberOfNew + 1);
            // Save the user.
                // userStats[0].save();
                // response.success("Successfully updated stats for user " + request.user.id);
            }
            else if(puzzleDifficulty=="additional"){
                userStats[0].increment("numberOfAdditional");
                userStats[0].set("points", points + additionalPoints);
                userStats[0].set("averageSteps", ((averageSteps*numberOfAdditional) + switches)/(numberOfAdditional + 1));
                //alert("This was set" + ((averageSteps*numberOfAdditional) + switches)/(numberOfAdditional + 1));
                //console.log("averageSteps: " + averageSteps + " :numberOfAdditional: "+numberOfAdditional + " :switches: "+switches);
                userStats[0].set("averageTime", ((averageTime*numberOfAdditional) + time)/(numberOfAdditional + 1));
            // Save the user.
 
                // userStats[0].save();
                // response.success("Successfully updated stats for user " + request.user.id);
            }
            objectToBeSavedArray.push(userStats[0]);
            Parse.Object.saveAll(objectToBeSavedArray, {
            success: function(objs) {
                response.success("Completed stats for user " + request.user.id);
            },
            error: function(error) { 
                // an error occurred...
            }
            });
    }
    else{
            var newUSer = Parse.Object.extend("Statistics");
            var user = new newUSer();
            var numberOfNew = 1;
      
              
            if(puzzleDifficulty=="easy"){
                user.increment("easy");
                user.set("points", easyPoints);
            }
            else if(puzzleDifficulty=="normal"){
                user.increment("normal");
                user.set("points", normalPoints);
            }
            else if(puzzleDifficulty=="hard"){
                user.increment("hard");
                user.set("points", hardPoints);
            }
            else if(puzzleDifficulty=="veryHard"){
                user.increment("veryHard");
                user.set("points", veryHardPoints);
            }
            else if(puzzleDifficulty=="noLimit"){
                user.increment("noLimit");
                user.set("points", noLimitPoints);
            }
            else if(puzzleDifficulty=="additional"){
                user.increment("numberOfAdditional");
                user.set("points", additionalPoints);
                numberOfNew = 0;
                user.set("averageSteps", switches);
                user.set("averageTime", time);
            }
            user.set("numberOfNew", numberOfNew);
            user.set("userPointer", userPointer);
 
            user.save(null,{
              success:function(person) { 
 
                var newUserPointer = new Parse.User;
                newUserPointer.id = request.user.id;
                newUserPointer.set("stats", person);  
 
 
                newUserPointer.save();
                alert("Used this instead");
                response.success(person);
              },
              error:function(error) {
                response.error("The error is" + error + error.message);
              }
            });
        }    
        
    },
    error: function(error) {
        //couldn't find the user so created new entry in Statistics
        //couldn't find the user so created new entry in Statistics
        var newUSer = Parse.Object.extend("Statistics");
        var user = new newUSer();
        var numberOfNew = 1;
  
          
        if(puzzleDifficulty=="easy"){
            user.set("easy", easyPoints);
            user.set("points", easyPoints);
        }
        else if(puzzleDifficulty=="normal"){
            user.set("normal", normalPoints);
            user.set("points", normalPoints);
        }
        else if(puzzleDifficulty=="hard"){
            user.set("hard", hardPoints);
            user.set("points", hardPoints);
        }
        else if(puzzleDifficulty=="veryHard"){
            user.set("veryHard", veryHardPoints);
            user.set("points", veryHardPoints);
        }
        else if(puzzleDifficulty=="noLimit"){
            user.set("noLimit", noLimitPoints);
            user.set("points", noLimitPoints);
            user.set("easy", easy);
            user.set("points", easy);
        }
        else if(puzzleDifficulty=="normal"){
            user.set("normal", normal);
            user.set("points", normal);
        }
        else if(puzzleDifficulty=="hard"){
            user.set("hard", hard);
            user.set("points", hard);
        }
        else if(puzzleDifficulty=="veryHard"){
            user.set("veryHard", veryHard);
            user.set("points", veryHard);
        }
        else if(puzzleDifficulty=="noLimit"){
            user.set("noLimit", noLimit);
            user.set("points", noLimit);
        }
        else if(puzzleDifficulty=="additional"){
            user.set("numberOfAdditional", additionalPoints);
            user.set("points", additionalPoints);
            numberOfNew = 0;
            user.set("averageSteps", switches);
            user.set("averageTime", time);
        }
        user.set("userPointer", userPointer);
 
        user.save(null,{
          success:function(person) { 
 
            var newUserPointer = new Parse.User;
            newUserPointer.id = request.user.id;
            newUserPointer.set("stats", person);  
            newUserPointer.save();
            alert("ran till here!!!!!");
 
            response.success(person);
          },
          error:function(error) {
            response.error(error + error.message);
          }
        });
          
        response.error("Could not find user with user " + userPointer);
    }
  });
    
});
  
 
 
/**Function to add the user as a recipient to our tutorial messageId
*Parameters: 
*userid - the object Id of the new user**/
Parse.Cloud.define("tutorialMessage", function(request, response) {
    //make sure the user is logged in
    if (!request.user) {
        response.error("Must be signed in to call Cloud function statistics.");
        return;
    }
    Parse.Cloud.useMasterKey();
    //Puzzle Category points
    var user = new Parse.User;
    user.id = request.user.id;
    //var messageId = "7CpLuBBTQQ"; //old message
    var messageId = "SeLYVu3fM0"; //new message
    var newUserPointer = new Parse.User;
    var objectToBeSavedArray = [];
    newUserPointer.id = request.user.id;
    newUserPointer.add("puzzlesSolved", "OYY02x2JPS"); //set the first puzzle solved as the user id
    newUserPointer.add("featuredSolved", "OYY02x2JPS");
    /**Create a entry for the user in the Statistics table*/
    //setting up the query
    var messages = Parse.Object.extend("Messages");
    var query = new Parse.Query(messages);
    query.equalTo("objectId",  messageId);
    query.first({
    success: function(tutorialMessage) {
        var recipient = tutorialMessage.relation("recipients");
        recipient.add(user);
         
        objectToBeSavedArray.push(tutorialMessage);
         
         
    },
    error: function(error) {
        //couldn't find the user so created new entry in Statistics
        response.error("Could not find user with user " + request.user.id);
    }
  });
 
 
    var newUSerStats = Parse.Object.extend("Statistics");
    var userStats = new newUSerStats();
    userStats.set("userPointer", request.user);
    userStats.set("points", 0);
    userStats.set("averageSteps", 0);
    userStats.set("averageTime", 0);
    userStats.set("easy", 0);
    userStats.set("hard", 0);
    userStats.set("veryHard", 0);
    userStats.set("noLimit", 0);
    userStats.set("normal", 0);
    userStats.set("numberOfAdditional", 0);
    userStats.set("numberOfFeatured", 0);
    userStats.set("featuredAverageSteps", 0);
    userStats.set("numberOfNew", 0);
    userStats.set("featuredAverageTime", 0);
    userStats.save();
    userStats.save(null,{
          success:function(person) { 
 
            /**Add the statistics object id to the user's stats column on the _User table**/
             
            newUserPointer.set("stats", person); 
            objectToBeSavedArray.push(newUserPointer);
            //initialize solved arrays for featured and additional
           // alert("stats pointer set");
            Parse.Object.saveAll(objectToBeSavedArray, {
            success: function(objs) {
                response.success("Successfully updated user " + request.user.id);
            },
            error: function(error) { 
                // an error occurred...
                response.error("Could not save changes to user " + request.user.id);
            }
            });
             
            //response.success(person);
          },
          error:function(error) {
            response.error(error + error.message);
          }
        });
 
 
     
 
   
});