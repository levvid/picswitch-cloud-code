/**
 * Picswitch
 * phoneVerification.js
 *
 * Author(s): Jiacong Xu
 * Created: Apr-13-2015
 *
 * Summary: This module handles phone number verifications, including requests
 * sent by user, confirmation requests received from third party API, CheckMobi,
 * and Parse related queries.
 *
 * Verification process should be as follows:
 * 1. Front-end checks whether phone number entered by user is already taken.
 * 2. User sends PVRequest to obtain a request object. Save that object locally.
 * 3. User sends PVValidate to validate with pin. This will return whether the
 *    number is successfully verified.
 */
 
 
/**
 * Constants
 */
const PV_SECRET_KEY = "7567B13A-E738-48E0-AF1A-1F3C56D79720";
const PV_CONTENT_TYPE = "application/json;charset=utf-8";
 
 
/**
 * This function requests a number verification. If a user already has the
 * provided number, that user will have his number invalidated on validation.
 *
 * request.params.phoneNumber (string): number being verified in international
 * format.
 * request.params.language (string): optional, language for the verification
 * prompt. Eg: en-US.
 * request.params.type (string): optional, validation type. Default is
 * reverse_cli. Specify sms for text message validation.
 * 
 * This returns a validation object, which should be passed into PVValidate.
 */
Parse.Cloud.define("PVRequest", function(request, response) {
  // Defining parameters for the request
  var num = request.params.phoneNumber;
  var lang = request.params.language;
  var t = request.params.type === "sms"? "sms" : "reverse_cli";
  var onSuccess = function(httpResponse) {
    response.success(httpResponse.data.id);
  };
  var onError = function(httpResponse) {
    response.error("Error with response code: " + httpResponse.status
                    + " and subcode: " + httpResponse.text);
  };
   
  // Configuring and sending the request
  Parse.Cloud.httpRequest({
    method:"POST",
    url:"https://api.checkmobi.com/v1/validation/request",
    headers:{"Content-Type":PV_CONTENT_TYPE, "Authorization":PV_SECRET_KEY},
    body:{number:num, type:t, language:lang},
    success:onSuccess,
    error:onError
  });
});
 
 
 
 
 
/**
 * This function attempts to validate a number using the validation info sent
 * by the user. This should be secure because even if user is able to construct
 * their own validation object, he will not be able to come up with the pin.
 *
 * request.params.pin (string): the validation pin.
 * request.params.request (string): the validation request, as returned by
 * PVRequest.
 *
 * This returns a boolean indicating whether number is validated.
 */
Parse.Cloud.define("PVValidate", function(request, response) {
  // Defining parameters for the request
  Parse.Cloud.useMasterKey();
   
  var validationId = request.params.request;
  var code = request.params.pin;
  var onSuccess = function(httpResponse) {
    if (httpResponse.data.validated) {
      // Search for any other players with this number and removes it.
      var query = new Parse.Query(Parse.User);
      query.equalTo("phoneNumber", httpResponse.data.number.substring(1));
       
      var onSuccess2 = function(users) {
        // Remove all the players' numbers that are the same to the validated
        for (var i = 0;  i < users.length; i++) {
          var user = users[i];
          user.save("phoneNumber", "");
        }
         
        response.success(true);
      };
       
      var onError2 = function(error) {
        response.error("Unable to query database! Error: " + error.message);
      };
       
      query.find({success:onSuccess2, error:onError2});
    } else {
      response.success(false);
    }
  };
   
  var onError = function(httpResponse) {
    response.error("Error with response code: " + httpResponse.status
                   + " and subcode: " + httpResponse.text);
  };
   
  // Configuring and sending the request
  Parse.Cloud.httpRequest({
    method:"POST",
    url:"https://api.checkmobi.com/v1/validation/verify",
    headers:{"Content-Type": PV_CONTENT_TYPE, "Authorization": PV_SECRET_KEY},
    body:{id:validationId, pin:code},
    success:onSuccess,
    error:onError
  });
});
 
 
 
 
/**
 * This function requests a number verification. If a user already has the
 * provided number, that user will have his number invalidated on validation.
 *
 * request.params.phoneNumber (string): number being verified in international
 * format.
 * request.params.language (string): optional, language for the verification
 * prompt. Eg: en-US.
 * request.params.type (string): optional, validation type. Default is
 * reverse_cli. Specify sms for text message validation.
 * 
 * This returns a Map with keys: 
 *                                 id: string which should be passed into PVValidate.
 *                                 hash: hash of the last 3 digits of the calling number
 */
Parse.Cloud.define("PVRequestAndroid", function(request, response) {
  // Defining parameters for the request
  var num = request.params.phoneNumber;
  var lang = request.params.language;
  var t = request.params.type === "sms"? "sms" : "reverse_cli";
  var onSuccess = function(httpResponse) {
 
    response.success({id:httpResponse.data.id, hash:httpResponse.data.pin_hash});
  };
  var onError = function(httpResponse) {
    response.error("Error with response code: " + httpResponse.status
                    + " and subcode: " + httpResponse.text);
  };
   
  // Configuring and sending the request
  Parse.Cloud.httpRequest({
    method:"POST",
    url:"https://api.checkmobi.com/v1/validation/request",
    headers:{"Content-Type":PV_CONTENT_TYPE, "Authorization":PV_SECRET_KEY},
    body:{number:num, type:t, language:lang},
    success:onSuccess,
    error:onError
  });
});