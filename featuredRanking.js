/***********************************************************************************************
*  PICSWITCH TECHNOLOGIES CONFIDENTIAL
*  ___________________________________
* 
*  PICSWITCH TECHNOLOGIES INC. 
*  Copyright (c) 2014, 2015 All Right Reserved.
* 
*
* Program           : PicSwitch Background job
*
* Author            : Gibson Levvid Mulonga
*
* Date created      : 16042015
*
* Purpose           : Handle push notifications
*                   : Handle PicSwitch Statistics for user puzzles
*                   : Make useful data available in a timely manner for advertisers
*                   : Track the preferences of our user base 
*                   : Handle featured puzzles statistics 
*
* 
*
* Revision History  :
*
* Version       Date(DDMMYYYY format)       Author                  Revision  
* v110          17032015                    Gibson Levvid Mulonga   Featured ranking
* v139          19042015                    Gibson Levvid Mulonga   Improving ranking algorithm 
*
************************************************************************************************/
 
 
/**Parse background job to handle featured ranking
* Parameters: */
Parse.Cloud.job("featuredRanking", function(request, status) {
    // Set up to modify user data
    Parse.Cloud.useMasterKey();
    var counter = 0;
    var numberOfWinners = 10; //number of winners that the company wants
    //setting up the query
    var featuredStatistics = Parse.Object.extend("FeaturedStatistics");
    var featuredPuzzles = Parse.Object.extend("FeaturedPuzzles");
    var query = new Parse.Query(featuredStatistics);
    var puzzle = "HQzVwWY2zA"; //puzzle object ID
    var userArray = new Array(numberOfWinners);
    var timeArray = new Array(numberOfWinners);
    var index;
    var numberOfUsers = 0;
    //fill up the time array with place holder values
    for(var i=0;i<numberOfWinners; i++){
        timeArray[i] = 1000;
    }
     
    //loop through all users who have done a particular puzzle and rank them 
    query.equalTo("puzzle", puzzle);
    query.find({
    success: function(users) {
    for(user in users){
        numberOfUsers = numberOfUsers + 1;
        var time = user.get("time");
        var steps = user.get("steps");
        index=0;
        while(index<numberOfWinners){
            if(timeArray[i]>time){ //time array used for comparison purposes
                if(i==0){
                    timeArray = timeArray.unshift(time); //add user's time to start of array
                    timeArray = timeArray.slice(0,-1); //remove 11th item
                    userArray = userArray.unshift(user.get("user")); //add user id to user array
                    userArray = userArray.slice(0,-1); //remove 11th item
                    break;
                }
                else if(i!=numberOfWinners){
                    //insert new value in arrays
                    timeArray = timeArray.slice(0,i-1).concat(time).concat(timeArray.slice(i, -1)); //old + new value + old minus last index
                    userArray = userArray.slice(0,i-1).concat(user.get("user")).concat(userArray.slice(i, -1));
                    break;
                    }
                else if(i==numberOfWinners){
                    timeArray = timeArray.slice(0,-1).concat(time); //remove 10th item then append new item
                    userArray = userArray.slice(0,-1).concat(user.get("user")); //remove 10th item then append new item as 10th or whatever depending on # of winners
                }
                }
            else{
                index++;
            }
        }
    }
 
    /**Update the positions of the winners in the featuredStatistics table
    **/
    for(var i=0; i<numberOfWinners; i++){
        //updating positions for the winners
        var queryStats = new Parse.Query(featuredStatistics);
        queryStats.equalTo("objectId", puzzle);
        queryStats.equalTo("user", userArray[i]);
        queryStats.first({
            success: function(insertPositions) {
                insertPositions.set("position", i+1);
                // Save the user.
                insertPositions.save(null, {
                success: function(insertPositions) {
                    // The user was saved successfully.
                    status.success("Updated position for user " + userArray[i]);
                },
                error: function(insertPositions, error) {
                    // The save failed.
                    // error is a Parse.Error with an error code and description.
                    status.error("Error during position update: "+ userArray[i] + " puzzle: " + puzzle);
                }
              });
               
            },
            error: function(error) {
                status.error("Could not find user:  " + userArray[i] + " associated with puzzle: " + puzzle);
            }
          });
        //
    }   
         
         
     
        status.success("Found the puzzle with id: " + puzzle);
    },
    error: function() {
      status.error("Could not find puzzle with id: " + puzzle);
    }
  });
 
 
 
 
    /**sending winners to featured table
    *Create relationship with featuredStatistics table
    *Positions are already a field in stats table*/
    var queryFeatured = new Parse.Query(featuredPuzzles);
   
        queryFeatured.equalTo("objectId", puzzle);
        queryFeatured.first({
            success: function(todaysFeaturedPuzzle) {
             
            var relation = todaysFeaturedPuzzle.relation("ranking");
            for(userID in userArray){
                relation.add(userID);
                 
            }
            todaysFeaturedPuzzle.set("numberOfUsers", numberOfUsers);
            todaysFeaturedPuzzle.save();
            status.success("Completed ranking for puzzle " + puzzle);
            },
            error: function() {
              status.error("Error during back job for puzzle: "+ puzzle);
            }
          });
     
});
 
 
 
 
 
 
 
// Parse.Cloud.job("fTest", function(request, status) {
// Parse.Cloud.useMasterKey();
 
// var newUser = new Parse.User;
// newUser.id = "KxCih62c2o";
 
// var Schedule = Parse.Object.extend("FeaturedStatistics");
// var schedule = new Schedule();
// var puzzleFeatured = Parse.Object.extend("FeaturedPuzzles");
// var puzzle = new puzzleFeatured();
// puzzle.id = "HQzVwWY2zA";
// newUser.add("featuredSolved", puzzle.id);
// schedule.set("user", newUser);
// schedule.set("puzzle", puzzle);
// schedule.set("time", 32);
// schedule.set("steps", 2);
// schedule.save();
// newUser.save();
// //updateFeaturedArray(newUser.id, puzzle.id, status);
// var stats = Parse.Object.extend("Statistics");
// var query = new Parse.Query(stats);
// query.equalTo("userPointer", newUser);
// query.find({
//   success: function(object) {
//     // Successfully retrieved the object.
//     if(object.length>0){
//         object[0].increment("numberOfFeatured");
//         var points = object.get("points");
//         object[0].set("points", points+5);
//         object[0].save();
//     }else{
//         var newUSer = Parse.Object.extend("Statistics");
//         var user = new newUSer();
//         user.set("points", 5);
//         user.set("numberOfFeatured", 1);
//         user.save();
//     }
     
//     status.success("Daily complete for runs on ");
//   },
//   error: function(error) {
//     alert("Error: " + error.code + " " + error.message);
//   }
// });
 
// // saving a record to the database
 
 
 
 
// });