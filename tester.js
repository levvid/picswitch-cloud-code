Parse.Cloud.job("tutorialMessageTest", function(request, response) {
    Parse.Cloud.useMasterKey();
    //Puzzle Category points
    var userID = "Yksdz7ubwm";
    var messageId = "7CpLuBBTQQ";
    //Statistics variables from user
    var newUser = new Parse.User();
    newUser.id = userID; 
     
    //setting up the query
    var messages = Parse.Object.extend("Messages");
    var query = new Parse.Query(messages);
    query.include("recipients");
    query.equalTo("objectId",  messageId);
    query.first({
    success: function(tutorialMessage) {
        var recipient = tutorialMessage.relation("recipients");
        recipient.add(newUser);
 
        // Save the user.
        tutorialMessage.save(null, {
        success: function(tutorialMessage) {
            // The user was saved successfully.
            response.success("Successfully sent a tutorial message to the user");
        },
        error: function(tutorialMessage, error) {
            // The save failed.
            // error is a Parse.Error with an error code and description.
            response.error("Could not add user to recipient list");
        }
      });
       
    },
    error: function(error) {
        //couldn't find the user so created new entry in Statistics
        response.error("There was an error while finding tutorial message: ", error);
    }
  });
   
});




Parse.Cloud.job("recipientsAdd", function(request, response) {
    Parse.Cloud.useMasterKey();
    
    var user1 = new Parse.User();
    user1.id = "V5EEDV3lD7";
    var user2 = new Parse.User();
    user2.id = "Yksdz7ubwm";
    //setting up the query
    var users = Parse.Object.extend("Statistics");
    var query = new Parse.Query(users);
    //query.include("recipients");
    query.equalTo("userPointer",  id);
    query.first({
    success: function(tutorialMessage) {
        alert("Retrieved: "  + tutorialMessage.get("firstCaption"));
        var recipient = tutorialMessage.relation("recipients");
        recipient.add(user1);
        recipient.add(user2);
 
        // Save the user.
        tutorialMessage.save(null, {
        success: function(tutorialMessage) {
            // The user was saved successfully.
            response.success("Successfully updated user ");
        },
        error: function(tutorialMessage, error) {
            // The save failed.
            // error is a Parse.Error with an error code and description.
            response.error("Could not save changes to user " );
        }
      });
       
    },
    error: function(error) {
        //couldn't find the user so created new entry in Statistics
        //couldn't find the user so created new entry in Statistics
        var newUSer = Parse.Object.extend("Statistics");
        var user = new newUSer();
 
        user.set("points", 1);
        user.set("numberOfPuzzles", 1);
        user.set("averageTime", 100);
        user.set("averageSteps", 100);
        user.set("userPointer", id);
 
        user.save(null,{
          success:function(person) { 
            response.success(person);
          },
          error:function(error) {
            response.error(error);
          }
        });
        response.error("Could not find user");
    }
  });
   
});


Parse.Cloud.job("makeFriends", function(request, response) {
    Parse.Cloud.useMasterKey();
    var gibson = new Parse.User;
    var yvette = new Parse.User;
    var marvo = new Parse.User;
    var jo = "FQNqPcFojY"; //joey
    
    var yve = "QgdAb75Lmn"; //robbie
    var gib = "Yksdz7ubwm"; //gibson
    var mar = "IPe4oGJxZA"; //jiacong
    gibson.id = gib;
    marvo.id = mar;
    yvette.id = yve;



   
    // var query = new Parse.Query(Parse.User);
    // query.equalTo("objectId", gib);  // find Joey
    // query.find({
    // success: function(user) {
    // gibson = user;

    // }
    // });


    // var query2 = new Parse.Query(Parse.User);
    // query2.equalTo("objectId", jiacong);  // find Robbie
    // query2.find({
    // success: function(user) {
    //     robbie = user;
       
    // }
    // });

    var relation = gibson.relation("friends");
    relation.add(marvo);
    relation.add(yvette);
    gibson.save();

    var relation = marvo.relation("friends");
    relation.add(yvette);
    relation.add(gibson);
    marvo.save();

    var relation = yvette.relation("friends");
    relation.add(marvo);
    relation.add(gibson);
    yvette.save();

    // var relation = robbie.relation("friends");
    // relation.add(joey);
    // robbie.save();

    

    

    
   
});



/**Function to add the user as a recipient to our tutorial messageId
*Parameters: 
*userid - the object Id of the new user**/ 
Parse.Cloud.job("tutorialMessage", function(request, response) {
    var userNew = new Parse.User;
    Parse.Cloud.useMasterKey();
    //Puzzle Category points
    var gib = "Yksdz7ubwm";
    var messageId = "7CpLuBBTQQ";
    //Statistics variables from user
    userNew.id = gib;
    
    //setting up the query
    var messages = Parse.Object.extend("Messages");
    var query = new Parse.Query(messages);
    query.equalTo("objectId",  messageId);
    query.first({
    success: function(tutorialMessage) {
        var recipient = tutorialMessage.relation("recipients");
        recipient.add(userNew);

        // Save the user.
        tutorialMessage.save(null, {
        success: function(tutorialMessage) {
            // The user was saved successfully.
            response.success("Successfully updated user " + userNew);
        },
        error: function(tutorialMessage, error) {
            // The save failed.
            // error is a Parse.Error with an error code and description.
            response.error("Could not save changes to user " + userNew);
        }
      });
      
    },
    error: function(error) {
        //couldn't find the user so created new entry in Statistics
        response.error("Could not find user with user " + userNew);
    }
  });
  
});


Parse.Cloud.job("featuredStatistics", function(request, response) {
    //make sure the user is logged in
    Parse.Cloud.useMasterKey();
    //Puzzle Category points
    var puzzleId = "HQzVwWY2zA";
    var time = 300;
    var steps = 45;
    //Statistics variables from user
    var newUser = new Parse.User;
    newUser.id = "Yksdz7ubwm"; 
     
    // var featuredStatistics = Parse.Object.extend("FeaturedStatistics");
    // var userPuzzleEntry = new featuredStatistics();
    // userPuzzleEntry.set("user", "Yksdz7ubwm");
    // userPuzzleEntry.set("puzzle", puzzleId);
    // userPuzzleEntry.set("time", time);
    // userPuzzleEntry.set("steps", steps);
    // userPuzzleEntry.save(null,{
    // success:function(user) { 
    // response.success("Successfully updated featuredStatistics");
    // },
    // error:function(error) {
    // response.error("Did not work");
    // }
    // });


    var PersonClass = Parse.Object.extend("FeaturedStatistics");
    var person = new PersonClass();

    person.set("user", newUser);
    person.set("puzzle", puzzleId);
    person.set("time", time);
    person.set("steps", steps);
    person.save(null,{
      success:function(person) { 
        response.success("YAP");
      },
      error:function(error) {
        response.error("NOPE" + error);
      }
    });




    

   
}); 





Parse.Cloud.define("ffeaturedStatistics", function(request, response) {
    //make sure the user is logged in
    if (!request.user) {
        response.error("Must be signed in to call Cloud function Featured Statistics.");
        return;
    }
    Parse.Cloud.useMasterKey();
    //Puzzle Category points
    var puzzleId = request.params.puzzleId;
    var time = request.params.time;
    var steps = request.params.steps;
     
    //setting up the query
    var featuredStatistics = Parse.Object.extend("FeaturedStatistics");
    var userPuzzleEntry = new featuredStatistics();
    userPuzzleEntry.set("user", request.user);
    userPuzzleEntry.set("puzzle", puzzleId);
    userPuzzleEntry.set("time", time);
    userPuzzleEntry.set("steps", steps);
    userPuzzleEntry.save(null,{
    success:function(userPuzzleEntry) { 
        updateFeaturedArray(request.user.id, puzzleId, response);
        updateStatsTable(request.user.id, response);
        response.success(userPuzzleEntry);
    },
    error:function(error) {
    response.error(error);
    }
    });


   
}); 



function updateFeaturedArray(userID, puzzleId, response) {
  var query = new Parse.Query(Parse.User);
    query.equalTo("objectId",  userID);
    query.first({
    success: function(user) {
        user.add("featuredSolved", puzzleId);
        // Save the user.
        user.save(null, {
        success: function(message) {
            // The user was saved successfully.
            response.success("Successfully updated user " + request.user.id);
        },
        error: function(message, error) {
            // The save failed.
            // error is a Parse.Error with an error code and description.
            response.error("Could not save changes to user " + request.user.id);
        }
      });
      
    },
    error: function(error) {
        //couldn't find the user so created new entry in Statistics
        response.error("Could not find user with user " + request.user.id);
    }
  });
}


function updateStatsTable(userID, response){
    //update stats table
    var stats = Parse.Object.extend("FeaturedStatistics");
    var userStats = new Parse.Query(stats); //stats query
    userStats.equalTo("userPointer",  userID);
    userStats.find({
    success: function(user) {
        if(user.length>0){
            user[0].increment("numberOfFeatured");
            var points = user[0].get("points");
            user[0].set("points", points+5);
        }else{
            var newUser = Parse.Object.extend("Statistics");
            var user = new newUser();
            user.set("points", 5);
            user.set("numberOfFeatured", 1);
            user.save(null,{
              success: function(object) { 
                response.success(object.id);  // or just object, to send the whole object back.
                
              },
              error: function(error) { 
                response.error(error);
              }
            });

        }
        
        
        

        // Save the user.
        user.save(null, {
        success: function(message) {
            // The user was saved successfully.
            response.success("Successfully updated user " + request.user.id);
        },
        error: function(message, error) {
            // The save failed.
            // error is a Parse.Error with an error code and description.
            response.error("Could not save changes to user " + request.user.id);
        }
      });
      
    },
    error: function(error) {
        //couldn't find the user so created new entry in Statistics
        response.error("Could not find user with user " + request.user.id);
    }
  });
}














Parse.Cloud.job("fTest", function(request, status) {
Parse.Cloud.useMasterKey();

var newUser = new Parse.User;
newUser.id = "KxCih62c2o";

var Schedule = Parse.Object.extend("FeaturedStatistics");
var schedule = new Schedule();
var puzzleFeatured = Parse.Object.extend("FeaturedPuzzles");
var puzzle = new puzzleFeatured();
puzzle.id = "HQzVwWY2zA";
newUser.add("featuredSolved", puzzle.id);
schedule.set("user", newUser);
schedule.set("puzzle", puzzle);
schedule.set("time", 32);
schedule.set("steps", 2);
schedule.save();
newUser.save();
//updateFeaturedArray(newUser.id, puzzle.id, status);
var stats = Parse.Object.extend("Statistics");
var query = new Parse.Query(stats);
query.equalTo("userPointer", newUser);
query.find({
  success: function(object) {
    // Successfully retrieved the object.
    if(object.length>0){
        object[0].increment("numberOfFeatured");
        var points = object.get("points");
        object[0].set("points", points+5);
        object[0].save();
    }else{
        var newUSer = Parse.Object.extend("Statistics");
        var user = new newUSer();
        user.set("points", 5);
        user.set("numberOfFeatured", 1);
        user.save();
    }
    
    status.success("Daily complete for runs on ");
  },
  error: function(error) {
    alert("Error: " + error.code + " " + error.message);
  }
});

// saving a record to the database




});







function updateStatsTable(userID, response){
    //update stats table
    var stats = Parse.Object.extend("Statistics");
    var userStats = new Parse.Query(stats); //stats query
    userStats.equalTo("userPointer",  userID);
    userStats.find({
    success: function(user) {
        if(user.length>0){
            user[0].increment("numberOfFeatured");
            var points = user[0].get("points");
            user[0].set("points", points+5);
            user[0].save();
        }else{
            var newUser = Parse.Object.extend("Statistics");
            var user = new newUser();
            user.set("points", 5);
            user.set("numberOfFeatured", 1);
            user.save(null,{
              success: function(object) { 
                //response.success(object.id);  // or just object, to send the whole object back.
                
              },
              error: function(error) { 
                response.error(error);
              }
            });

        }
        
        
        

        // Save the user.
        user.save(null, {
        success: function(message) {
            // The user was saved successfully.
            //response.success("Successfully updated user " + request.user.id);
        },
        error: function(message, error) {
            // The save failed.
            // error is a Parse.Error with an error code and description.
            response.error("Could not save changes to user " + request.user.id);
        }
      });
      
    },
    error: function(error) {
        //couldn't find the user so created new entry in Statistics
        response.error("Could not find user with user " + request.user.id);
    }
  });
}




Parse.Cloud.job("sendPushToUser", function(request, response) {
  var senderUser = new Parse.User;
  senderUser.id = "Yksdz7ubwm";
  var senderUsername = "BackEnd";
  var recipientUsername = "gibson";
  var message = "request.params.message";
 
  // Validate that the sender is allowed to send to the recipient.
  // For example each user has an array of objectIds of friends
 
  // Validate the message text.
  // For example make sure it is under 140 characters
  if (message.length > 40) {
  // Truncate and add a ...
    message = senderUsername + " says: \"" + message.substring(0, 28) + "...\"";
  }else if(message.length==""){
    message = "New picswitch from " + senderUsername;
    }
    else if(message==null){
        message = "new picswitch from " + senderUsername;
    }
    else {
        message = senderUsername + " says: \"" + message + "...\"";
    }
    
  
  // Send the push.
  // Find devices associated with the recipient user
  var recipientUser = new Parse.User();
  recipientUser.username = recipientUsername;
  var pushQuery = new Parse.Query(Parse.Installation);
  pushQuery.equalTo("username", recipientUsername);
  
  // Send the push notification to results of the query
  Parse.Push.send({
    where: pushQuery,
    data: {
      alert: message
    }
  }).then(function() {
      response.success("Push was sent successfully. " + message)
  }, function(error) {
      response.error("Push failed to send with error: " + error.message);
  });
});